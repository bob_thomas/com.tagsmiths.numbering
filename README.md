# README #

This DITA-OT plugin always inserts processing instructions into temporary topic files that 1) Indicate the bookmap structural type (chapter, appendix, or topic) and 2) the sequence number of the topic (for example,  2.4.3 or A.1.4). 

If <data name="numbered-titles" value="true"/> is present in the bookmeta, all topic titles are edited so that they begin with sequence numbers. Caution! Titles furnished by topic-less navtitles (for example, topichead, or chapter with a title that is set through navtitle) get counted but their titles don't receive numbers.